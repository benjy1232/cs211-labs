#include<iostream> //Declaration of library iostream
using namespace std; //Use of namespace std

void matrixMultiplication(int rowA, int colA, int rowB, int colB, int matrixA[][20], int matrixB[][20]); //Declares function matrixMultiplication with all those parameters

int main(void) { //Declaration of function main with no input
    //Declaration of all variables
    const int SIZE = 20; //Declaration of constant int SIZE of 20
    int matrixA[SIZE][SIZE]; //Declaration of matrixA with SIZE rows and SIZE columns
    int matrixB[SIZE][SIZE];//Declaration of matrixB with SIZE rows and SIZE columns
    int resultant[SIZE][SIZE];//Declaration of resultant with SIZE rows and SIZE columns
    int actSizeAC, actSizeAR; //Declaration of int actSizeAC and actSizeAR
    int actSizeBC, actSizeBR; //Declaration of int actSizeBC and actSizeBR
    int sum = 0;//Declaration of int sum with value 0

    //Clears all matrices and sets all elements in them equal to 0
    for (int clearR = 0; clearR < SIZE; clearR++) { //For loop to clear rows
        for (int clearC = 0; clearC < SIZE; clearC++) { //For loop to clear columns
            matrixA[clearR][clearC] = 0;//matrixA element clearR,clearC is equal to 0
            matrixB[clearR][clearC] = 0;//matrixB element clearR,clearC is equal to 0
            resultant[clearR][clearC] = 0;//resultant element clearR,clearC is equal to 0
        }
    }

    //Creation of user generated matrix for matrixA
    cout << "Input Matrix 1: \n"; //Outputs "Input Matrix 2: " followed by a new line
    cout << "Input number of rows: "; //Outputs "Input number of rows: "
    cin >> actSizeAR; //Takes user input and stores it in variable actSizeAR
    cout << "Input number of columns: "; //Outputs "Input number of columns"
    cin >> actSizeAC; //Takes user input and stores it in varaible actSizeAC
    for (int i = 0; i < actSizeAR; i++) { //For loop that repeats actSizeAR amount of times
        for (int j = 0; j < actSizeAC; j++) { //For loop that repeats actSizeAC amount of times
            cout << "Enter element [" << i << "," << j << "]: "; //Outputs "Enter element [i,j]: "
            cin >> matrixA[i][j];//Takes user input and stores it in [i,j] in matrixA
        }
    }

    //Creation of user generated matrix for matrixB   
    cout << "Input Matrix 2: \n";//Outputs "Input Matrix 2: " followed by a new line
    cout << "Input number of rows: ";//Outputs "Input number of rows: "
    cin >> actSizeBR;//Takes user input and stores it in variable actSizeBR
    cout << "Input number of columns: ";//Outputs "Input number of columns"
    cin >> actSizeBC;//Takes user input and stores it in varaible actSizeBC
    for (int k = 0; k < actSizeBR; k++) {//For loop that repeats actSizeBR amount of times
        for (int l = 0; l < actSizeBC; l++) {//For loop that repeats actSizeBC amount of times
            cout << "Enter element [" << k << "," << l << "]: ";//Outputs "Enter element [k,l]: "
            cin >> matrixB[k][l];//Takes user input and stores it in [k,l] in matrixB
        }
    }

    //Matrix Multiplication
    if (actSizeAC == actSizeBR) {//Checks to see if matrix multiplication can happen by comparing columns of matrixA and rows of matrixB
        matrixMultiplication(actSizeAR, actSizeAC, actSizeBR, actSizeBC, matrixA, matrixB); //Goes to function matrixMultiplication
    }
    else {
        cout << "Resultant Matrix: Matrices are not compatible"; //Outputs "Resultant Matrix: Matrices are not compatible"
    }
    return 0; //Exits code with return code 0
}

void matrixMultiplication(int rowA, int colA, int rowB, int colB, int matrixA[][20], int matrixB[][20]) { //Beginning of function matrixMultiplication
    int resultant[20][20];
    for (int row = 0; row < 20; row++) { //For loop that repeats 20 times
        for (int col = 0; col < 20; col++) { //For loop that repeats 20 times
            resultant[row][col] = 0; //Sets element row, col of resultant equal to 0
        }
    }
    for (int i = 0; i < rowA; i++) { //Repeats rowA amount of times
        for (int j = 0; j < colB; j++) { //Repeats colB amount of times
            for (int k = 0; k < colA; k++) { //Repeats colA amount of times
                resultant[i][j] = resultant[i][j] + matrixA[i][k] * matrixB[k][j]; //Does the multiplication by saving it into element [i,j] into array resultant. Multiplies element matrixA[i,k] by matrix[k, j]
            }
        }
    }

    cout << "Resultant Matrix: " << rowA << " x " << colB << endl; //Outputs "Resultant matrix: *number of rows in matrix 1* x *number of columns in matrix 2*" followed by a new line
    for (int a = 0; a < rowA; a++) { //For loop that repeats actSizeAR amount of times
        for (int b = 0; b < colB; b++) { //For loop that repeats actSizeBC amount of times
            cout << resultant[a][b] << " "; //Outputs resultant element [a,b] followed by a space
        }
        cout << endl; //Outputs a new line
    }
}