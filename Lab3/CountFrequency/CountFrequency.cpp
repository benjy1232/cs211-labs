#include<iostream> //Declaration of library iostream in use
using namespace std; //Using namespace std

void check(int arr[], int actSize);

int main(void){ //Declaration of function main
    //Declaration of all the variables to be used
    const int SIZE=20; //Declaration of constant int SIZE of size 20
    int array[SIZE]; //Declaration of array "array" size SIZE
    int actSize; //Declaration of int actSize
    
    //Set all elements in array equal to 0
    for(int i=0; i<SIZE; i++){//For loop that repeats SIZE amount of times
        array[i]=0; //Sets element i of "array" equal to 0
    }

    //Asking for size of array and catch case for when actSize>SIZE
    cout << "Please enter array size: "; //Outputs "Please enter array size: "
    cin >> actSize; //Takes the input and stores it in variable actSize
    while(actSize>SIZE){//While actSize is greater than SIZE (acting as an if statement) then do the following
        cout << "Out of range. Please enter a number below 20\n"; //Outputs "Out of range. Please enter a number below 20" followed by new line
        cin >> actSize; //Takes user input and stores it in actSize
    }

    //Creation of array
    for(int j=0; j<actSize; j++){ //For loop that repeats actSize amount of times
        cout << "Please enter element " << j << ": "; //Outputs "Please enter element *j*: "
        cin >> array[j]; //Takes user input and stores it in element j of array
    }
    check(array, actSize);

}

void check(int array[], int actSize){
    const int SIZE=20;
    int compare[SIZE]={};//Declaration of array "compare" size SIZE
    int frequency[SIZE]={}; //Declaration of array "frequency" size SIZE
    bool flag=0; //Declaration of boolean flag set to 0
    int x=0; //Declaration of in x with value 0
    for(int k=0; k<actSize; k++){//For loop that repeats actSize amount of times
        flag = 0; //flag set to 0 at the beginning of loop to make sure it only goes into if statement if it did not match any previous numbers
        for(int l=0; l<actSize; l++){ //For loop that repeats l amount of times
            if(array[k]==compare[l]){//Compares arr[k] with num[l] and if they are equal flag is set to 1 to skip if statement at the end of the major loop
                frequency[l]++; //Increment frequency element l by 1
                flag=1; //Sets flag equal to as to skip if statement outside of this for loop
            }
        }
        if(flag==0){//if the previous loop did not any matches then it will go into this statement
            compare[x]=array[k];//sets num[x] equal to arr[k]
            frequency[x]++; //Increments frequency element x by 1
            x++; //increments x as to not overwrite any previous elements
        }
    }
    //Outputs the results
    cout << "number --> count \n"; //Outputs "number --> count" followed by a newline
    for(int m=0; m<x; m++){ //For loop that repeats x amount of times
        cout << compare[m] << "--> " << frequency[m] << endl; //Outputs the number and how many times it occurred in the program
    }
}