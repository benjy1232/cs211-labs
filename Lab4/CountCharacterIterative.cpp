#include<iostream>
using namespace std;

int main(void){
	const int SIZE = 1000;
	char string[SIZE] = {};
	char lookFor;
	int i=0;
	int freq=0;
	cout << "Enter a string: ";
	cin.getline(string, SIZE);
	while(string[i]!='\0'){
		i++;
	}
	cout << "Enter character to search for: ";
	cin >> lookFor;
	for(int j=0; j<i; j++){
		if(string[j]==lookFor){
			freq++;
		}
	}
	cout << "Character \'" << lookFor << "\' occurs " << freq << " times in given string \"";
	for(int k=0; k<i; k++){
		cout << string[k];
	}
	cout << "\"" << endl;
	return 0;
}
