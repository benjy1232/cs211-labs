#include<iostream> //Declares library iostream being used
using namespace std; //Declares namespace std in use

void clear(char string[], int end, int initial=0){ //Declaration of void function "clear" that takes in string, end, and initial, and initial=0 if no input given
	if(initial==end) //If initial = end
		return; //Exits function
	string[initial]=0; //Else, set string[initial] = 0
	clear(string, end, initial+1); //Go through this function again except increase value of initial by 1
	return; //Exits the function
}

int found(char string[],char looking, int end, int initial = 0, int ret=0){ //Declaration of function found with return type int with inputs string (character array), looking (char), initial, end and ret all of type int. ret will start with value 0 if no input is given
	if(initial==end) //If initial=end
		return ret; //Exits function and returns the final value of ret
	if(string[initial] == looking) //If string[initial] = looking then
		return found(string, looking, end, initial+1, ret+1); //Go through function again but increase initial and ret by 1 and return that value to previous function
	else //If string[initial]!=looking then
		return found(string, looking, end, initial+1, ret); //Go through function again but increase initial and returns the current value of ret into the next function
}

void printString(char string[], int end, int initial=0){ //Declaration of function printString with inputs string (character array), end, and initial of type int. Initial will start with value 0 if no input is given
	if(initial == end) //If initial = end
		return; //Exits function
	cout << string[initial]; //Outputs value of string[initial]
	printString(string, end, initial+1); //Goes through function again
	return; //Exits function
}

int main(void){ //Declaration of function void
	const int SIZE=1000; //Declaration of const int SIZE of 1000
	char string[SIZE] = {}; //Declaration of character array string of size SIZE that should be initialized with 0
	char lookFor; //Declaration of char lookFor
	int ret; //Declaration of int ret
	clear(string, SIZE); //Goes to function clear with inputs string and SIZE
	cout << "Enter a string: "; //Outputs "Enter a string: "
	cin.getline(string, SIZE); //Inputs the whole line and stores it in character array string
	cout << "Enter character to search for: "; //Outputs "Enter character to search for: "
	cin >> lookFor; //Stores user input in char lookFor
	ret = found(string, lookFor, SIZE); //ret = output of function found with inputs string, lookFor, SIZE);
	cout << "Character \'" << lookFor << "\' occurs " << ret << " times in the given string \""; //Outputs "Character 'lookFor' occurs *ret* times in given string "
	printString(string, SIZE); //Goes to function printString with inputs string and SIZE
	cout << "\"" << endl; //Outputs a final quotation mark and new line
	return 0; //Exits the program
}
