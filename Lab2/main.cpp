#include<iostream> //Declare library being used as iostream
using namespace std; //Declares namespace as std as to not write out std::cout, etc
const int SIZE=20; //Declares constant int of size 20 with variable name SIZE to put into array

int main(void){ //Declaration of function "main"
	//This section is the declaration of all variables and arrays to be used in this program
	int arrA[SIZE]; //Declares array arrA of size SIZE
	int arrB[SIZE]; //Declares array arrB of size SIZE
	int actSize; // Declares variable actSize
	int resultant[SIZE]; //Declares array resultant of size SIZE

	//This little section makes all elements in array 0
	for(int i=0; i<SIZE; i++){ //for loop that goes on from i=0 to i=SIZE-1 while i increments by 1 each time
		arrA[i]=0; //Sets element i of array arrA to be 0
		arrB[i]=0; //Sets element i of array arrB to be 0
		resultant[i]=0; //Sets element i of array resultant to be 0
	}

	//This area asks for how many elements to be in each array
	cout << "Please enter number of elements: ";  //Outputs "Please enter number of elements: "
	cin >> actSize; //Takes the input and puts it into variable "actSize"

	//Ensuring the program doesn't crash because actSize is not of valid size
	while(actSize>SIZE){ //A while loop that declares the number of elements is out of range and will continue putting out the error until a valid response is entered
		cout << "Out of range, Please Enter number of elements from 1 up to 20\n"; //Outputs "Out of range, Please Enter number of elements from 1 up to 20" followed by a new line aka \n
		cin >> actSize; //Takes the input and puts it into variable "actSize"
	}

	//Asks user to input elements into arrays
	cout << "Please enter elements for FIRST array: \n"; //Outputs "Please enter elements for FIRST array: " followed by a new line
	for(int j=0; j<actSize; j++){ //A for loop to going from j=0 to actSize-1; increments j each time
		cin >> arrA[j]; //Inputs that number into array arrA element j
	}
	cout << "Please enter elements for SECOND array: \n"; //Outputs "Please enter elements for SECOND array: " followed by a new line
	for(int l=0; l<actSize; l++){//A for loop to going from l=0 to actSize-1; increments l each time
		cin >> arrB[l]; //Inputs that number into array arrb element l

	}

	//Calculates the Resultant and outputs the result
	cout << "The RESULT is: "; //Outputs "The RESULT is: " with no new line following it
	for(int k=0; k<actSize; k++){ //A for loop going from k=0 all the way to actSize-1 and incrementing k by 1 each time
		resultant[k]=arrA[k]*arrB[k]; //Calculates the result of the multiplication of current element k and puts that into the array
		if(k<actSize-1) //Adds a comma after every number besides the last
			cout << resultant[k] << ", "; //Outputs the result that was just found and follows it with a comma and space
		else
			cout << resultant[k]; //Outputs the final result
	}
	return 0; //Exits the program with return code 0
}
