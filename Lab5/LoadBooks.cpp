#include<iostream> //Declaration of the use of library iostream
#include<fstream> //Declaration of the use of library fstream
using namespace std; //Declaration of the use of namespace std

struct book{ //Declaration of structure book
    char title[1000]; //Book contains a title: character array of 1000
    char author[1000]; //Book contains an author: character array of 1000
    int year_released; //Book contains a year_released: int
};

int main(void){ //Declaration of main function
    char filename[1000] = {}; //Declaration of filename variable: character array size 1000
    int num_books; //Declaration of the integer num_books
    ifstream ifs; //Declaration of ifs: type ifstream
    cout << "Enter filename: "; //Outputs "Enter filename: "
    cin >> filename; //Takes input and stores it into character array "filename"
    ifs.open(filename); //Uses "filename" as path and opens it using ifstreams method "open"

    ifs >> num_books; //Reads the first number in the file and stores it in num_books
    cout << "Books found: " << num_books << endl; //Outputs "Books found: "

    book * books = new book[num_books]; //Creates a pointer pointing to a dynamically created array of type book of size num_books
    for(int i = 0; i < num_books || !ifs.eof() ;i++){ //For however many books it finds or until the end of the file is found it will do this
         ifs.getline(books[i].title, 1000); //Reads the next line and stores that into books[i].title
         if(books[i].title[0] == '\0'){ //If the first character of books[i].title is equal to '\0' do it again
             ifs.getline(books[i].title, 1000); //Reads the next line and stores that into books[i].title
         }
         ifs.getline(books[i].author, 1000); //Reads the next line and stores that value into books[i].author
         ifs >> books[i].year_released; //Reads the final value but doesn't read "\0" which is why there is the need for the if-statement in this loop
    }

    ifs.close(); //closes the file so it is no longer being read

    for(int j = 0; j < num_books; j++){ //For however many books are in this list do this
        cout << "Book " << j + 1 << ":\n"; //Outputs "Book x: " where x is the current index of the book + 1 because we don't 0 index lists, followed by a new line
        cout << "Title: " << books[j].title << endl; //Outputs "Title: " followed by the title of the book of the current index
        cout << "Author: " << books[j].author << endl; //Outputs "Author: " followed by the author of the book of the current index
        cout << "Year: " << books[j].year_released << endl << endl; //Outputs "Year: " followed by the year it was released of the current index
    }
    delete[] books; //Deletes the dynamically created array
}