#include<iostream> //library being used is iostream
using namespace std; //Namespace being used is std so compiler knows to use std:: then command

int main(void) { //Main function declared with input void
	int input,x=0; //Declares variables input and x. X is declared with input equal to 0
	cout << "Please enter a number: "; //Outputs "Please enter a number: "
	cin >> input; //Number being input to console
	cout << "List of even numbers:\n"; //Outputs "List of even numbers:" followed by a new line
	for (int i = 1; i <= input; i++) { // for-loop going through every number below and equal to input
		if (i % 2 == 0) {//checks for even numbers by checking for remaineders when divided by 2 or mod 2
			cout << i << ", "; //Even number being output
			x = 1; //if x=1 then it will skip if statement after loop
		}
	}
	if (x == 0) { //Checks for x equal to 0
		cout << "None"; //When x is equal to 0 there are no even numbers below it
	}
	cout << endl; //Creates a new line
	cout << "List of odd numbers:\n"; //Prints out "List of odd numbers:" followed by a new line
	for (int i = 0; i <= input; i++) {//for-loop going through every number below and equal to input
		if (i % 2 == 1) {//if when divided by 2 there is a remaineder, then it is odd
			cout << i << ", "; //outputs said odd number
		}
	}
	return 0; //exits the program
}